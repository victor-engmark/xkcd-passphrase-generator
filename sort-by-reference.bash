#!/usr/bin/env bash

set -o errexit -o errtrace -o noclobber -o nounset -o pipefail
shopt -s failglob inherit_errexit

if [[ $# -ne 2 ]]; then
    cat >&2 << EOF
Usage: ./sort-by-reference.bash REFERENCE INPUT

    For all words in INPUT, print them in the order they appear in REFERENCE.
    Print the remaining words in INPUT afterwards. This results in a minimal
    diff between REFERENCE and INPUT, which is useful for version control, while
    keeping all the words in INPUT.

Example: ./sort-by-reference.bash old-words.txt new-words.txt > words.txt
EOF
fi

reference="$1"
input="$2"

# Print lines in INPUT which are also in REFERENCE (in the order they appear in REFERENCE)
grep --file="$input" --line-regexp "$reference"

# Print lines in INPUT which are _not_ in REFERENCE (in the order they appear in INPUT)
grep --file="$reference" --invert-match --line-regexp "$input"
