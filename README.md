# XKCD passphrase generator

Simply run **`./generate.sh`** to generate a passphrase based on the XKCD
method:
[![Password Strength comic by Randall Munroe](https://imgs.xkcd.com/comics/password_strength.png)](https://xkcd.com/936/)

By default this uses the **2,048** most popular words in the current language,
if supported[^supported-languages]. See [`generate.sh`](generate.sh) for how to
override these.

[^supported-languages] Supported languages include:

-  English
-  Norwegian Bokmål

The English word list is based on
[a diceware word list by EFF](https://www.eff.org/deeplinks/2016/07/new-wordlists-random-passphrases),
specifically tailored for generating strong but easy-to-remember passphrases.

The Norwegian Bokmål word list is from
[Wiktionary](https://en.wiktionary.org/w/index.php?title=Wiktionary:Frequency_lists/Norwegian_Bokm%C3%A5l_wordlist&oldid=76476256).

Adapting this to _other languages_ would be easy. If you can provide a list of
at least the 2,048 most popular words in a language (sorted by decreasing
popularity) I can adapt this program for your language. Please file an issue and
include a link to the word list.

Notes:

-  The original comic quoted “~44 bits of entropy” for a four word passphrase
   like “correct horse battery staple”, making it simple to assume that the
   dictionary contained 2,048 words (2^(44/4)). The dictionary used is unknown,
   but even though the first three words are all in
   [the word list](words/en.txt), “staple” is actually word number _333,333_ in
   [Peter Norvig’s list](http://norvig.com/ngrams/count_1w.txt).
-  [The security of this approach has been discussed elsewhere](https://security.stackexchange.com/q/6095/1220).
   User beware.

## License

The code is licensed under the [GNU Affero GPL v3+](LICENSE).
