#!/bin/sh

set -o errexit -o nounset

words="${1-2048}"

locale="${LC_ALL:-$LANG}"
language="${locale%%_*}"

printf '%s\n' "$(head -n "$words" "$(dirname -- "$0")/words/${language}.txt" | shuf --head-count=4 | tr '\n' ' ')"
